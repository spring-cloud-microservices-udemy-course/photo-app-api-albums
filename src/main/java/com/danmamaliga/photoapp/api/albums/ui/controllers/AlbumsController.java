package com.danmamaliga.photoapp.api.albums.ui.controllers;

import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import java.lang.reflect.Type;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.danmamaliga.photoapp.api.albums.data.AlbumEntity;
import com.danmamaliga.photoapp.api.albums.service.AlbumsService;
import com.danmamaliga.photoapp.api.albums.ui.model.AlbumResponseModel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/users/{id}/albums")
public class AlbumsController {

	@Autowired
	AlbumsService albumsService;
	Logger log = LoggerFactory.getLogger(this.getClass());

	@PreAuthorize("hasRole('ADMIN') or principal == #id")
	@GetMapping
	public List<AlbumResponseModel> userAlbums(@PathVariable String id) {

		List<AlbumResponseModel> returnValue = new ArrayList<>();

		List<AlbumEntity> albumsEntities = albumsService.getAlbums(id);

		if (albumsEntities == null || albumsEntities.isEmpty()) {
			return returnValue;
		}

		Type listType = new TypeToken<List<AlbumResponseModel>>() {
		}.getType();

		returnValue = new ModelMapper().map(albumsEntities, listType);
		log.info("Returning %s albums");
		return returnValue;
	}
}
