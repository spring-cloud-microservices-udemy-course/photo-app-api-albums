FROM eclipse-temurin:21 
VOLUME /tmp 
COPY target/PhotoAppApiAlbums-0.0.1-SNAPSHOT.jar PhotoAppApiAlbums.jar 
ENTRYPOINT ["java","-jar","PhotoAppApiAlbums.jar"]